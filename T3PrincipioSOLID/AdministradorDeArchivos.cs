﻿
using System.IO;

namespace T3PrincipioSOLID
{
    public class AdministradorDeArchivos
    {
        public virtual void SalvarImagen(byte[] BytesImagen, string nombre)
        {
            FileStream FlujoDeArchivo = new FileStream(nombre, FileMode.Create);
            BinaryWriter EscribirBinario = new BinaryWriter(FlujoDeArchivo);
            try
            {
                EscribirBinario.Write(BytesImagen);
            }
            finally
            {
                FlujoDeArchivo.Close();
                EscribirBinario.Close();
            }

        }
    }
}
