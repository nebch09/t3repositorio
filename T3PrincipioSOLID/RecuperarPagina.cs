﻿using HtmlAgilityPack;
using System;
using System.IO;
using System.Net;


namespace T3PrincipioSOLID
{
    public class RecuperarPagina
    {
        internal HtmlDocument ObtenerPagina()
        {
            var solicitud = (HttpWebRequest)WebRequest.Create("http://www.ofifacil.com/");

            HttpWebResponse respuesta = (HttpWebResponse)solicitud.GetResponse();
            HtmlDocument documento = new HtmlDocument();

            var secuenciaDeResultado = respuesta.GetResponseStream();
            documento.Load(secuenciaDeResultado);

            return documento;
        }

        public byte[] ObtenerImagen(string url)
        {
            byte[] BytesImagen;
            HttpWebRequest solicitudDeImagen = (HttpWebRequest)WebRequest.Create(url);
            WebResponse respuestaImagen = solicitudDeImagen.GetResponse();

            Console.WriteLine("descargar la imagen");

            Stream secuenciaDeRespuesta = respuestaImagen.GetResponseStream();

            using (BinaryReader lectorBinario = new BinaryReader(secuenciaDeRespuesta))
            {
                BytesImagen = lectorBinario.ReadBytes((int)respuestaImagen.ContentLength);
                lectorBinario.Close();
            }
            secuenciaDeRespuesta.Close();
            respuestaImagen.Close();

            return BytesImagen;
        }
    }
}
