﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace T3PrincipioSOLID
{
   public class Rastreador
    {
        RecuperarPagina recuperarPagina = new RecuperarPagina();

        private AdministradorDeArchivos administradorDeArchivos;

        public AdministradorDeArchivos administradorDeArchivo
        {
            get
            {
                if (this.administradorDeArchivos == null)
                {
                    this.administradorDeArchivos = new AdministradorDeArchivos();

                }

                return administradorDeArchivos;

            }
            set { administradorDeArchivos = value; }
        }

        public void Proceso()
        {

            var documento = recuperarPagina.ObtenerPagina();

            Console.WriteLine("Datos de Rastreo");

            var cuerpo = documento.DocumentNode.ChildNodes.Single(node => node.Name == "html").ChildNodes.Single(node => node.Name == "body");

            var IdContenedor = cuerpo.ChildNodes.Single(node => node.Id == "contenedor");

            var IdCabecera = IdContenedor.ChildNodes.Single(node => node.Id == "cabecera");

            var imagenes = IdCabecera.ChildNodes["div"].ChildNodes.Where(node => node.Name == "img");

            var urls = new List<string>();

            foreach (var imagen in imagenes)
            {
                urls.Add(imagen.Attributes["src"].Value);
            }

            

            foreach (var url in urls)
            {
                byte[] imageBytes = recuperarPagina.ObtenerImagen(@"http://www.ofifacil.com/" + url);
                FileStream fs = new FileStream("../../imagenes/" + url.Split('/').Last(), FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                try
                {
                    bw.Write(imageBytes);
                }
                finally
                {
                    fs.Close();
                    bw.Close();
                }
            }
            
            Console.WriteLine("done!");

            Console.ReadLine();

        }
    }
}
