﻿using HtmlAgilityPack;
using System;
using System.Net;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace T3
{
   public class Rastreador
    {
       public void Proceso()
       {

           var solicitud = (HttpWebRequest)WebRequest.Create("http://www.ofifacil.com/");



           HttpWebResponse respuesta = (HttpWebResponse)solicitud.GetResponse();
           HtmlDocument documento = new HtmlDocument();

           var FlujoDelResultado = respuesta.GetResponseStream();
           documento.Load(FlujoDelResultado);

           Console.WriteLine("Rastreando Imagenes");

           var cuerpo = documento.DocumentNode.ChildNodes.Single(node => node.Name == "html").ChildNodes.Single(node => node.Name == "body");

           var IdContenedor = cuerpo.ChildNodes.Single(node => node.Id == "contenedor");

           var IdCabecera = IdContenedor.ChildNodes.Single(node => node.Id == "cabecera");

           var imagenes = IdCabecera.ChildNodes["div"].ChildNodes.Where(node => node.Name == "img");

           var urls = new List<string>();

           foreach (var imagen in imagenes)
           {
               urls.Add(imagen.Attributes["src"].Value);
           }

           foreach (var url in urls)
           {

               byte[] imageBytes;
               HttpWebRequest imageRequest = (HttpWebRequest)WebRequest.Create(@"http://www.ofifacil.com/" + url);
               WebResponse imageResponse = imageRequest.GetResponse();

               Console.WriteLine("downloading image");

               Stream responseStream = imageResponse.GetResponseStream();

               using (BinaryReader br = new BinaryReader(responseStream))
               {
                   imageBytes = br.ReadBytes((int)imageResponse.ContentLength);
                   br.Close();
               }
               responseStream.Close();
               imageResponse.Close();

               FileStream fs = new FileStream("../../images/" + url.Split('/').Last(), FileMode.Create);
               BinaryWriter bw = new BinaryWriter(fs);
               try
               {
                   bw.Write(imageBytes);
               }
               finally
               {
                   fs.Close();
                   bw.Close();
               }

           }

           Console.WriteLine("done!");

           Console.ReadLine();

       }
    }
}




    

